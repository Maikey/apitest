package com.epam.training.test_backend.model;

import com.epam.training.test_backend.framework.CreateJSONBody;

public class Bet extends CreateJSONBody{
	
	private Integer id;
	private String description;
	private String type;
	
	public Bet() {}
	public Bet(	 Integer id, String description, String type) 
	{
		this.id=id;
		this.description=description;
		this.type=type;
	}
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	// here comes the bet model
	// here comes the bet model
	/*[ 
	   { 
	      "id":15,
	      "description":"Fradi Ute win bet",
	      "type":"Win bet"
	   },
	   { 
	      "id":16,
	      "description":"Fradi Ute goals bet",
	      "type":"All goals bet"
	   }
	]*/
	
}
